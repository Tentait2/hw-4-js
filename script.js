"use strict"

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Аякс это подход к получению данных с других источников добавляя её к нам на страницу без нужды перезагрузки


	const URL = "https://ajax.test-danit.com/api/swapi/films"

class Requests {
	get(url) {
		return fetch(url).then((response) => response.json());
	}
}

class StarWars {
	getFilms() {
		const request = new Requests();
		return request.get(URL).then((data)=> {
			let filmsSort = data.sort((a,b) =>{
				return a.episodeId - b.episodeId
			})

			let filmsList = document.createElement("ul")
			let films = filmsSort.map((el) => {
				let {name, episodeId, openingCrawl, characters} = el
				let filmItem = document.createElement("li")
				let filmDescr = document.createElement("p")
				let heroesList = document.createElement("ul")
				let heroes = characters.map(element => {
					const request = new Requests()
					return request.get(element).then(data => {
						let heroName = document.createElement("li");
						let { name } = data
						heroName.textContent = name;
						heroesList.append(heroName);
					})
				})
				Promise.all(heroes).then(() => {
					filmItem.textContent = `Episode number: ${episodeId} - ${name}`;
					filmDescr.textContent = openingCrawl
					filmItem.append(filmDescr)
					filmItem.append(heroesList)
				})
				return filmItem
			})
			filmsList.append(...films)
			return filmsList
		})
	}
}


const starwars = new StarWars();
starwars.getFilms().then((data)=> document.body.append(data));





